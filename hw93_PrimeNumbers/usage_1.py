"""Homework 9 Task 3 implementation."""

import prime_numbers as pn


for num in (-1, 0, 1, 2, 3, 4, 5, 19, 3557, 123456789):
    print(f"is {num} prime number? - {pn.is_prime(num)}")

for num in pn.prime_numbers(10):
    print(num, end=' ')
