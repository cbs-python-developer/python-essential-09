"""Homework 9 Task 3 implementation."""

from prime_numbers import is_prime, prime_numbers


for num in (-1, 0, 1, 2, 3, 4, 5, 19, 3557, 123456789):
    print(f"is {num} prime number? - {is_prime(num)}")

for num in prime_numbers(10):
    print(num, end=' ')
