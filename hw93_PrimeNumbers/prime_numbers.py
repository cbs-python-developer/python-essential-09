"""Homework 9 Task 3 implementation."""

import math
from typing import Generator


def is_prime(number: int) -> bool:
    """Check whether number is prime."""
    if number < 2:
        return False
    for idx in range(2, int(math.sqrt(number)) + 1):
        if number % idx == 0:
            return False
    return True


def prime_numbers(n: int) -> Generator:
    """Return first N prime numbers."""
    counter = 0
    number = 2
    while counter < n:
        if is_prime(number):
            yield number
            counter += 1
        number += 1
