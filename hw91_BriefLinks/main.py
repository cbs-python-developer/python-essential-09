"""Homework 9 Task 1 implementation."""

from db_handler import DBHandler
import user_interaction as ui


def main(db_file: str) -> None:
    """Display the main menu and perform the actions."""

    db = DBHandler(db_file)
    actions = [
        "Quit",
        "Add a new link",
        "Show all links",
        "Find a link",
    ]

    ui.display_message("\nWelcome to the BRIEF LINKS SERVICE!")
    while True:
        command = ui.choose_from_list(actions, "\nMAIN MENU")

        match command:
            case 'Quit':
                ui.display_message("See you soon!")
                break
            case 'Add a new link':
                full_link = ui.get_string("Enter the full link")
                brief_link = ui.get_string("Enter the link brief name")
                db.add_link(full_link, brief_link)
                ui.display_link(brief_link, full_link)
            case 'Show all links':
                ui.display_all_links(db.get_all_links())
            case 'Find a link':
                brief_link = ui.get_string("Enter the link brief name")
                try:
                    full_link = db.find_link(brief_link)
                except KeyError:
                    full_link = 'Nothing found'
                ui.display_link(brief_link, full_link)


if __name__ == '__main__':

    db_file_name = './db-brief-links'
    main(db_file_name)
