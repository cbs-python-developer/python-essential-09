"""Homework 9 Task 1 implementation."""

import shelve
from pathlib import Path
from typing import Iterator


class DBHandler:
    """Class to interact with a database."""

    def __init__(self, db_file: str) -> None:
        self.db_file = db_file
        # create DB file if not exists
        db_file_path = Path(self.db_file)
        if not db_file_path.is_file():
            db_file_path.parent.mkdir(parents=True, exist_ok=True)
            with shelve.open(self.db_file):
                pass

    def add_link(self, full_link: str, brief_link: str) -> None:
        """Add a new link to the database."""
        with shelve.open(self.db_file) as db:
            db[brief_link] = full_link

    def get_all_links(self) -> Iterator[tuple[str, str]]:
        """Return all links in the database."""
        with shelve.open(self.db_file) as db:
            for brief_link, full_link in db.items():
                yield brief_link, full_link

    def find_link(self, brief_link: str) -> str:
        """Find a link in the database."""
        with shelve.open(self.db_file) as db:
            return db[brief_link]
