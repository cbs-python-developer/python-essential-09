"""Homework 9 Task 1 implementation."""

from typing import Iterator


def get_string(prompt: str) -> str:
    """Get a string from input."""
    answer = input(f"{prompt}: ")
    return answer


def get_number(prompt: str) -> int:
    """Get an integer from input."""
    while True:
        try:
            number = int(input(f"{prompt}: "))
            return number
        except ValueError as err:
            print(f"!Invalid input: {err}")


def choose_from_list(items: list, prompt: str) -> str:
    """Choose an item from the list by its index."""
    items_str = '\n\t'.join([
        f"'{idx}' - {item}" for idx, item in enumerate(items)
    ])
    while True:
        item_idx = get_number(f"{prompt}:\n\t{items_str}\n")
        try:
            return items[item_idx]
        except IndexError as err:
            print(f"!Invalid input: {err}")


def display_message(message: str) -> None:
    """Display a message to the user."""
    print(message)


def display_link(brief_link: str, full_link: str) -> None:
    """Display a link to the user."""
    print(f"{brief_link} --> {full_link}")


def display_all_links(links: Iterator[tuple[str, str]]) -> None:
    """Display links to the user."""
    for brief_link, full_link in links:
        display_link(brief_link, full_link)
